use crate::parse_tarpit_log;

mod action;
mod datetime;
mod loglevel;
mod logentry;
mod socket;

#[test]
fn parsing_example_log() {
    let input = include_str!("./tarpit.log");
    let parsed = parse_tarpit_log(input).unwrap();
    dbg!(parsed);
}
