use std::net::{Ipv4Addr, SocketAddrV4};

use chrono::NaiveDate;

use crate::{parsing::parse_log_line, Action, LogLevel, TarpitLogEntry};

#[test]
fn parsing_log_line_event() {
    let input =
        "2022-01-06 17:55:11 INFO    TarpitServer: Client ('159.65.200.85', 49698) connected";
    let (rest, parsed) = parse_log_line(input).unwrap();

    let expected = TarpitLogEntry::Event {
        ip: SocketAddrV4::new(Ipv4Addr::new(159, 65, 200, 85), 49698u16),
        timestamp: NaiveDate::from_ymd_opt(2022, 1, 6)
            .unwrap()
            .and_hms_opt(17, 55, 11)
            .unwrap(),
        action: Action::Connect,
        log_level: LogLevel::Info,
    };
    assert_eq!(parsed, expected);
    assert!(rest.is_empty());
}

#[test]
fn parsing_log_line_message() {
    let input = "2022-01-06 17:55:11 INFO    Main: Server startup completed.";
    let (rest, parsed) = parse_log_line(input).unwrap();

    let expected = TarpitLogEntry::Message {
        timestamp: NaiveDate::from_ymd_opt(2022, 1, 6)
            .unwrap()
            .and_hms_opt(17, 55, 11)
            .unwrap(),
        log_level: LogLevel::Info,
        issuer: String::from("Main"),
        message: String::from("Server startup completed."),
    };
    assert_eq!(parsed, expected);
    assert!(rest.is_empty());
}
